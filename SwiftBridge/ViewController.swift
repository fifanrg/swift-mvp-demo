//
//  ViewController.swift
//  SwiftBridge
//
//  Created by jianingren on 5/01/2016.
//  Copyright © 2016 Jianing Ren. All rights reserved.
//

import UIKit
import Socket_IO_Client_Swift




class ViewController: UIViewController,UIWebViewDelegate {
    var webview:UIWebView!
    var bridge:WebViewJavascriptBridge!
    
    
    @IBAction func mvpDemoButtonClicj(sender: AnyObject) {
        let bookingListViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("bookingListView") as! BookingListViewController
        self.presentViewController(bookingListViewController, animated: true) { () -> Void in
            
        }
    }
    
    
    @IBAction func onCallJSButtonClick(sender: AnyObject) {
        /*
        let testDic:[String:String] = ["method":"connectSocket","content":"hello"]
        self.bridge.callHandler("testJavascriptHandler", data:testDic)
        */
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        /*
            This is the swift-javascript bridge
        */
        
        /*
        webview = UIWebView(frame: CGRectMake(0, 200, UIScreen.mainScreen().bounds.width, UIScreen.mainScreen().bounds.height))
        self.view.addSubview(webview)
        //      print("webview :%@",webview)
        
        WebViewJavascriptBridge.enableLogging()
        
        self.bridge = WebViewJavascriptBridge(forWebView: webview, webViewDelegate: self) {
            [unowned self](data, responseCallback) -> Void in
             print("self : %@  data :%@",self,data)
        }
        
        
        self.bridge.registerHandler("testObjcCallback") { (data, responseCallback) -> Void in
            print("testBridgeCall callback :%@",data)
        }
        
        self.bridge.send("A string sent from ObjC before Webview has loaded.") { (data) -> Void in
            print("objc got response! %@",data)
        }
        
        self.bridge.callHandler("testJavascriptHandler", data: "hello")
        
        let htmlPath = NSBundle.mainBundle().pathForResource("ExampleApp", ofType: "html")
        let appHtml:String? = try! String(contentsOfFile: htmlPath!, encoding: NSUTF8StringEncoding)
        let baseURL = NSURL(fileURLWithPath: htmlPath!)
        self.webview.loadHTMLString(appHtml!, baseURL: baseURL)
        
        self.bridge.send("A string sent from ObjC before Webview has loaded.") { (data) -> Void in
            print("Objc got response ! %@",data)
        }
        */
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    
    
    
    @IBAction func navigateToMainPage(sender: AnyObject) {
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc: UIViewController = storyboard.instantiateViewControllerWithIdentifier("baseView") as UIViewController
        self.presentViewController(vc, animated: true, completion: nil)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

