//
//  BookingDetailViewController.swift
//  SwiftBridge
//
//  Created by jianingren on 7/01/2016.
//  Copyright © 2016 Jianing Ren. All rights reserved.
//

import UIKit



protocol BookingDetailOperation{
    func bookingDeleted(booking : Booking)
}


class BookingDetailViewController: UIViewController {
    
    var booking:Booking?
    var bookingDelegate : BookingDetailOperation?
    var presenter:BookingDetailPresenter?
    
    
    @IBOutlet weak var passengerNameLabel: UILabel!
    @IBOutlet weak var pickupLocationLabe: UILabel!
    @IBOutlet weak var destinationAddressLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.renderBookingInfo()
        
        
        /*
            Note: 
            This ViewController should have its own presenter and model(viewModel) if it has business logic to deal with
            I am just so lazy to create another one
        */
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func renderBookingInfo(){
        passengerNameLabel.text = booking!.passengerName
        pickupLocationLabe.text = booking!.pickupAddress
        destinationAddressLabel.text = booking!.destinationAddress
    
    }
    
    
    @IBAction func ignoreBooking(sender: AnyObject) {
        self.bookingDelegate?.bookingDeleted(booking!)
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
