//
//  BookingListViewController.swift
//  SwiftBridge
//
//  Created by jianingren on 7/01/2016.
//  Copyright © 2016 Jianing Ren. All rights reserved.
//

import UIKit

class BookingListViewController: UIViewController{
    
    var presenter:BookingListPresenter?
    var dataSource:NSArray = [Booking]()
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let booking1:Booking = Booking(passengerName: "johnny", pickupAddress: "Mascot", destionation: "Internation airport")
        let booking2:Booking = Booking(passengerName: "Mark", pickupAddress: "Townhall", destionation: "castle Hill")
        let booking3:Booking = Booking(passengerName: "Xiao", pickupAddress: "City", destionation: "Olympic Park")
        let model:NSArray = [booking1,booking2,booking3]
        tableView.delegate = self
        tableView.dataSource = self
        /*
            Note : This model can also be ViewModel , the idea of using model or viewModel is to use reative cocoa to bind model(viewModel) with UI, and then manipulate model(ViewModel) ONLY in presenter.
            Separating business logic from ViewController
        */
        presenter = BookingListPresenter(model: model, view: self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func showBookingList(sender: AnyObject) {
        self.presenter?.displayBookings()
    }
}


/*
    Note:
    It is better to have all the protocols as extensions to make code much clearer code than OC
    However we can do it in old fashion to put protocol into main ViewController
*/
extension BookingListViewController : UITableViewDataSource{
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataSource.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell:UITableViewCell=UITableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: "mycell")
        let booking = dataSource[indexPath.row] as! Booking
        let textString:String = "Passenger Name :  \(booking.passengerName) from:  + \(booking.pickupAddress) to:  \(booking.destinationAddress)"
        cell.textLabel?.numberOfLines = 0
        cell.textLabel?.text = textString
        return cell
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 100.0
    }
}


extension BookingListViewController : UITableViewDelegate{
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let booking:Booking = dataSource[indexPath.row] as! Booking
        let bookingDetailViewController:BookingDetailViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("bookingDetail") as! BookingDetailViewController
        bookingDetailViewController.bookingDelegate = self
        bookingDetailViewController.booking = booking
        self.presentViewController(bookingDetailViewController, animated: true) { () -> Void in
        }
    }
}


/*
    Note:
    This is to link presenter and viewController together via a protocol as recommonded from swift programming given protocol in swift2 is really powerful
    We can have not only
*/
extension BookingListViewController:BookingListViewProtocol{
    func displayBookings(bookings: NSArray) {
        self.dataSource = bookings
        self.tableView.reloadData()
    }
}


extension BookingListViewController : BookingDetailOperation {
    func bookingDeleted(booking: Booking) {
        if(self.dataSource.containsObject(booking)){
            let tempArray = NSMutableArray(array: self.dataSource) as NSMutableArray
            tempArray.removeObject(booking)
            self.dataSource = Array(tempArray) as! [Booking]
            self.tableView.reloadData()
        }
    }
    
    
}
