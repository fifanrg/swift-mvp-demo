//
//  IGBaseOculusViewController.swift
//  SwiftBridge
//
//  Created by jianingren on 6/01/2016.
//  Copyright © 2016 Jianing Ren. All rights reserved.
//

import UIKit



class IGBaseOculusViewController: UIViewController {
    
    @IBOutlet weak var avaiableJobsContainerScrollView: UIScrollView!
    @IBOutlet weak var bookingPanelContainer: UIView!
    
    var bookView1:IGAvaiableBookingView!
    var bookView2:IGAvaiableBookingView!
    var bookView3:IGAvaiableBookingView!
    let buddleDiameter:CGFloat = UIScreen.mainScreen().bounds.size.width + 100.0
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    @IBAction func startAnimation(sender: AnyObject) {
        
        
        UIView.animateWithDuration(1.5, delay: 0, options: UIViewAnimationOptions.CurveEaseInOut, animations: { () -> Void in
//            self.bookView1.animatingContentFading()
            }) { (completed : Bool) -> Void in
                self.bookView1.removeAllSubElements()
                
                let scaleValue:CGFloat =  self.bookingPanelContainer.frame.size.height / self.buddleDiameter
                let translate:CGAffineTransform = CGAffineTransformMakeTranslation(-700, -700)
                let scale:CGAffineTransform = CGAffineTransformMakeScale(scaleValue, scaleValue)
                let transform:CGAffineTransform = CGAffineTransformConcat(translate, scale)
                UIView.beginAnimations("MoveAndScaleAnimation", context: nil)
                UIView.setAnimationCurve(UIViewAnimationCurve.EaseInOut)
                UIView.setAnimationDuration(1)
                self.bookView1.transform = transform
                UIView.commitAnimations()
                
                
                
                /*
                UIView .animateWithDuration(2.0,delay: 0, options: UIViewAnimationOptions.CurveEaseInOut, animations:{ () -> Void in
                    
                    let scaleValue:CGFloat =  self.bookingPanelContainer.frame.size.height / self.buddleDiameter

                    let translate:CGAffineTransform = CGAffineTransformMakeTranslation(0, 0)
                    let scale:CGAffineTransform = CGAffineTransformMakeScale(scaleValue, scaleValue)
                    let transform:CGAffineTransform = CGAffineTransformConcat(translate, scale)
                
//                    self.bookView1.transform = CGAffineTransformMakeScale(scaleValue, scaleValue)
                }) { (completed : Bool) -> Void in
                    UIView .animateWithDuration(2.0) { () -> Void in
                        self.bookView1.center = self.bookingPanelContainer.center
//                        self.avaiableJobsContainerScrollView.contentOffset = CGPointMake(0, self.bookView2.center.y - self.buddleDiameter/2)
                    }
                }
*/
        }
        
        /*
        UIView.animateWithDuration(1.5, animations: { () -> Void in
            self.bookView1.animatingContentFading()
            let scaleValue:CGFloat =  self.bookingPanelContainer.frame.size.height / self.buddleDiameter
            self.bookView1.transform = CGAffineTransformMakeScale(scaleValue, scaleValue)
            
            }) {(completed) -> Void in
                UIView .animateWithDuration(2.0) { () -> Void in
                    self.avaiableJobsContainerScrollView.contentOffset = CGPointMake(0, self.bookView2.center.y - self.buddleDiameter/2)
                }
        }
        */
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.avaiableJobsContainerScrollView.backgroundColor = UIColor.clearColor()
        self.avaiableJobsContainerScrollView.scrollEnabled = false
        self.simulateBookingList()
        
    }
    
    
    
    func simulateBookingList(){
        
        bookView1 = NSBundle.mainBundle().loadNibNamed("bookingBubbleView", owner: self, options: nil)[0] as? IGAvaiableBookingView
        bookView1.frame = CGRectMake(self.view.center.x - buddleDiameter/2, 0, buddleDiameter, buddleDiameter)
        var endPointY =  bookView1.frame.size.height
        let marginY:CGFloat = 70.0
        bookView2 = NSBundle.mainBundle().loadNibNamed("bookingBubbleView", owner: self, options: nil)[0] as? IGAvaiableBookingView
        bookView2.frame = CGRectMake(self.view.center.x - buddleDiameter/2, endPointY+marginY, buddleDiameter, buddleDiameter)
        
        endPointY = bookView2.frame.origin.y + bookView2.frame.size.height
        bookView3 = NSBundle.mainBundle().loadNibNamed("bookingBubbleView", owner: self, options: nil)[0] as? IGAvaiableBookingView
        bookView3.frame = CGRectMake(self.view.center.x - buddleDiameter/2, endPointY+marginY, buddleDiameter, buddleDiameter)
        
        
        /*
        let marginY:CGFloat = 70.0
        for(var i = 0; i < 3 ; i++){
            let bookView:IGAvaiableBookingView! = NSBundle.mainBundle().loadNibNamed("bookingBubbleView", owner: self, options: nil)[0] as? IGAvaiableBookingView
            if(i == 0){
                bookView.frame = CGRectMake(self.view.center.x - buddleDiameter/2, CGFloat(i) * buddleDiameter, buddleDiameter, buddleDiameter)
            }else{
                bookView.frame = CGRectMake(self.view.center.x - buddleDiameter/2, CGFloat(i) * (buddleDiameter + marginY), buddleDiameter, buddleDiameter)
            }
            self.avaiableJobsContainerScrollView.addSubview(bookView)
        }
        
        self.avaiableJobsContainerScrollView.contentSize = CGSizeMake(self.view.frame.width, 3.0 * buddleDiameter + (marginY * 2.0))
        */
        
        self.avaiableJobsContainerScrollView.addSubview(bookView1)
        self.avaiableJobsContainerScrollView.addSubview(bookView2)
        self.avaiableJobsContainerScrollView.addSubview(bookView3)
        
        
        self.avaiableJobsContainerScrollView.contentSize = CGSizeMake(self.view.frame.width, bookView3.frame.origin.y + bookView3.frame.size.height + marginY)
        self.bookingPanelContainer.layer.cornerRadius = self.bookingPanelContainer.frame.width/2
        self.bookingPanelContainer.layer.borderWidth = 3
        self.bookingPanelContainer.layer.borderColor = UIColor.blackColor().CGColor
        self.bookingPanelContainer.layer.masksToBounds = true
        self.bookingPanelContainer.backgroundColor = UIColor.whiteColor().colorWithAlphaComponent(0.7)
    
        
        

        
    }
    
    
    
    
    
    @IBAction func goBackToPreviousPag(sender: AnyObject) {
        
        self.dismissViewControllerAnimated(true) { () -> Void in
            
        }
        
        
    }
    
}
