//
//  BookingListViewProtocol.swift
//  SwiftBridge
//
//  Created by jianingren on 8/01/2016.
//  Copyright © 2016 Jianing Ren. All rights reserved.
//

import Foundation

protocol BookingListViewProtocol {
    
    func displayBookings(bookings:NSArray)
    
}