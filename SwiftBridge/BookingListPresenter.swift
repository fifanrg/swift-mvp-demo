//
//  BookingListPresenter.swift
//  SwiftBridge
//
//  Created by jianingren on 7/01/2016.
//  Copyright © 2016 Jianing Ren. All rights reserved.
//

import Foundation


class BookingListPresenter: NSObject {
    
    /*
        Not necessarily have to be model, can be ViewModel as well when they are several models needed for viewController
    */
    var model:[Booking]!
    
    /*
        protocol-oriented to communicate from presenter to its binding viewController
    */
    
    var view:BookingListViewProtocol = BookingListViewController()
    
    override init(){
        super.init()
    }
    
    convenience init(model: NSArray, view:  BookingListViewProtocol) {
        self.init()
        self.model = model as! [Booking]
        self.view = view
    }
    
    func displayBookings(){
        view.displayBookings(model)
    }
}
