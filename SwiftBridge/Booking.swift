//
//  Booking.swift
//  SwiftBridge
//
//  Created by jianingren on 7/01/2016.
//  Copyright © 2016 Jianing Ren. All rights reserved.
//

import Foundation




class Booking: NSObject {
    
    var passengerName:String!
    var pickupAddress:String!
    var destinationAddress:String!
    
    override init(){
        super.init()
    }
    
    convenience init(passengerName:String,pickupAddress:String,destionation:String) {
        self.init()
        self.passengerName = passengerName
        self.pickupAddress = pickupAddress
        self.destinationAddress = destionation
    }
}