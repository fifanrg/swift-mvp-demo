//
//  IGAvaiableBookingView.swift
//  SwiftBridge
//
//  Created by jianingren on 6/01/2016.
//  Copyright © 2016 Jianing Ren. All rights reserved.
//

import UIKit

class IGAvaiableBookingView: UIView {
    
    
    
    @IBOutlet weak var awayTimeLabel: UILabel!
    
    @IBOutlet weak var requestedPickupLabel: UILabel!
    
    @IBOutlet weak var dropoffLabel: UILabel!
    
    
    override func layoutSubviews() {
        self.layer.borderWidth = 8
        self.layer.borderColor = UIColor.blackColor().CGColor
        self.layer.cornerRadius = self.frame.size.width/2.0
        self.layer.masksToBounds = true
        super.layoutSubviews()
    }
    
    
    
    func animatingContentFading(){
//        self.removeConstraints(self.constraints)
        self.awayTimeLabel.alpha = 0
        self.requestedPickupLabel.alpha = 0
        self.dropoffLabel.alpha = 0
    }
    
    
    
    func removeAllSubElements(){
        self.removeConstraints(self.constraints)
        layoutSubviews()
        self.awayTimeLabel.hidden = true
        self.requestedPickupLabel.hidden = true
        self.dropoffLabel.hidden = true
//        self.awayTimeLabel.removeFromSuperview()
//        self.requestedPickupLabel.removeFromSuperview()
//        self.dropoffLabel.removeFromSuperview()
        
        
        
    }
    
    
}
